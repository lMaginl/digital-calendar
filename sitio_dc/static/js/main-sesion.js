document.addEventListener("DOMContentLoaded", function() {
  var elems = document.querySelectorAll(".datepicker");
  var instances = M.Datepicker.init(elems, {
    container: "body",
    format: "dd/mm/yyyy",
    firstDay: 1,
    autoClose: true,
    setDefaultDate: true,
    i18n: {
      clear: "Limpiar",
      done: "Aceptar",
      cancel: "Cancelar",
      months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
      weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
      weekdaysAbbrev: ["D", "L", "M", "X", "J", "V", "S"]
    }
  });
});

document.addEventListener("DOMContentLoaded", function() {
  var elems = document.querySelectorAll(".timepicker");
  var instances = M.Timepicker.init(elems, {
    container: "body",
    autoClose: true,
    twelveHour: false
  });
});

document.addEventListener("DOMContentLoaded", function() {
  var elems = document.querySelectorAll(".profile-button");
  var instances = M.Dropdown.init(elems);
});

document.addEventListener("DOMContentLoaded", function() {
  var elems = document.querySelectorAll(".sidenav");
  var instances = M.Sidenav.init(elems);
});

window.$(".modal").modal();
