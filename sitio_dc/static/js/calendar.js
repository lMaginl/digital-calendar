/*
 * ================================================
 *             CALENDARIO
 *===================================
 */
let monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

let currentDate = new Date();
let currentDay = currentDate.getDate();
let monthNumber = currentDate.getMonth();
let currentYear = currentDate.getFullYear();

let month = document.getElementById("month");
let dates = document.getElementById("dates");

month.textContent = monthNames[monthNumber];

writeMonth(monthNumber);

function writeMonth(month) {
  for (let i = startDay(); i > 0; i--) {
    dates.innerHTML += `<div class="days prev_date">${getTotalDays(monthNumber - 1) - (i - 1)}</div>`;
  }

  for (let i = 1; i <= getTotalDays(month); i++) {
    if (i === currentDay) {
      dates.innerHTML += `<div class="days today">${i}</div>`;
    } else {
      dates.innerHTML += `<div class="days">${i}</div>`;
    }
  }
}

function getTotalDays(month) {
  if (month === -1) month == 11;

  if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
    return 31;
  } else if (month == 3 || month == 5 || month == 8 || month == 10) {
    return 30;
  } else {
    return isLeap() ? 29 : 28;
  }
}

function isLeap() {
  return (currentYear % 100 !== 0 && currentYear % 4 === 0) || currentYear % 400 === 0;
}

function startDay() {
  let start = new Date(currentYear, monthNumber, 1);
  return start.getDay() - 1 === -1 ? 6 : start.getDay() - 1;
}

function lastMonth() {
  if (monthNumber !== 0) {
    monthNumber--;
  } else {
    monthNumber = 11;
    currentYear--;
  }

  setNewDate();
}

function nextMonth() {
  if (monthNumber !== 11) {
    monthNumber++;
  } else {
    monthNumber = 0;
    currentYear++;
  }
}

function setNewDate() {
  currentDate.setFullYear(currentYear, monthNumber, currentDay);
  month.textContent = monthNames[monthNumber];

  dates.textContent = "";
  writeMonth(monthNumber);
}

/*
 * ================================================
 *             HORA
 *===================================
 */

function mostrarSaludo() {
  currentHour = currentDate.getHours();

  if (currentHour >= 0 && currentHour < 12) {
    textSay = "Buenos Días";
  }

  if (currentHour >= 12 && currentHour < 18) {
    textSay = "Buenas Tardes";
  }

  if (currentHour >= 18 && currentHour < 24) {
    textSay = "Buenas Noches";
  }

  document.getElementById("saludo").innerHTML = `<div class = saludo>${textSay}</div>`;
}
