from django.apps import AppConfig


class DgAppConfig(AppConfig):
    name = 'dg_app'
