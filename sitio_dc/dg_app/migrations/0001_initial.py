# Generated by Django 2.2.6 on 2019-11-04 23:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Recordatorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('diaPublicacion', models.DateTimeField(blank=True, null=True)),
                ('diaRecordatorio', models.DateField(blank=True, null=True)),
                ('horaRecordatorio', models.TimeField(blank=True, null=True)),
                ('titulo', models.CharField(max_length=20)),
                ('asunto', models.CharField(max_length=13)),
                ('descripcion', models.TextField(max_length=60)),
                ('autor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
