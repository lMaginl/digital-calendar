from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Recordatorio(models.Model):
    diaPublicacion = models.DateTimeField( blank=True, null=True)
    diaRecordatorio = models.DateField(blank=True, null=True)
    horaRecordatorio = models.TimeField(blank=True, null=True)
    titulo = models.CharField(max_length=20)
    asunto = models.CharField(max_length=13)
    descripcion = models.TextField(max_length=60)
    autor = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.titulo

    def crear(self):
        try:
            self.save()
            return True
        except:
            return False

    def modificar(self):
        try:
            self.save()
            return True
        except:
            return False

    def buscar_todos(self,user):
        listado = Recordatorio.objects.filter(autor=user)
        return listado

    def buscar(self,id):
        record = Recordatorio.objects.get(id=id)

        self.id = record.id
        self.diaRecordatorio = record.diaRecordatorio
        self.horaRecordatorio = record.horaRecordatorio
        self.titulo = record.titulo
        self.asunto = record.asunto
        self.descripcion = record.descripcion
        self.autor = record.autor

        return record

    def eliminar(self,id):
        try:
            self.buscar(id)
            self.delete()
            return True
        except:
            return False