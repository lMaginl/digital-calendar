from django.shortcuts import render, HttpResponse
from django.utils import timezone
from .models import Recordatorio
from django.shortcuts import render, get_object_or_404
from .forms import RecordatorioForm
from django.shortcuts import redirect
from django.contrib.auth.models import User, auth
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from datetime import datetime
from django.core import serializers
import json
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required


#Paginas
def index(request):
    return render(request,'digital_calendar/index.html')

def registro(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        password1 = request.POST['password1']
        email = request.POST['email']

        try:
            user = User.objects.create_user(username=username,password=password1,
            email=email,first_name=first_name,last_name=last_name)
            user.save()
            return redirect('/iniciar_sesion')
        except:
            return redirect('/registro')
    else:
        return render(request,'digital_calendar/registro.html')

def iniciar_sesion(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username,password=password)

        if user is not None:
            auth.login(request,user)
            return redirect('/home')
        else:
            messages.success(request,'true')
            return redirect('iniciar_sesion')
    else:
        return render(request,'digital_calendar/iniciar_sesion.html')

@login_required
def salir(request):
    auth.logout(request)
    return redirect('/')

#PERFIL USUARIO
@login_required
def perfil(request):
    usuario = request.user.username
    perfil = User.objects.filter(username=usuario)

    if request.method == 'POST':
        usuario = request.user

        if request.POST.get('nueva') == request.POST.get('confirm'):
            if usuario.check_password(request.POST.get('actual')):
                usuario.set_password(request.POST.get('nueva'))
                usuario.save()

                update_session_auth_hash(request, usuario)

                messages.success(request,'true')
            else:
                messages.error(request,'false')
        else:
            messages.error(request,'false')
    return render(request,'digital_calendar/Usuario/perfil.html', {'perfil':perfil})

#CRUD RECORDATORIOS
@login_required
def home(request):
    user = request.user
    reco = Recordatorio().buscar_todos(user)
    variables = {
        'reco':reco
    }

    #CREAR
    if request.method == 'POST':
        rec = Recordatorio()
        rec.diaPublicacion = timezone.now()
        rec.diaRecordatorio = request.POST.get('diaRecordatorio')
        rec.horaRecordatorio = request.POST.get('horaRecordatorio')
        rec.titulo = request.POST.get('titulo')
        rec.asunto = request.POST.get('asunto')
        rec.descripcion = request.POST.get('descripcion')
        user = request.user
        rec.autor = user

        try:
            rec.crear()
            return redirect('/home')
        except:
            return redirect('/home')
    return render(request, 'digital_calendar/home.html', variables)

#LISTAR TODOS
@login_required
def listar(request):
    user = request.user
    reco = Recordatorio().buscar_todos(user)
    variables = {
        'reco':reco
    }
    return render(request, 'digital_calendar/CRUD/listar.html', variables)

@login_required
def eliminar(request, id):
    reco = Recordatorio().buscar(id)

    try:
        reco.eliminar(id)
        mensaje = 'Eliminado Correctamente'
        messages.success(request,mensaje)
    except:
        mensaje = 'No se pudo Eliminar'
        messages.error(request,mensaje)

    return redirect('/listar')

@login_required
def modificar(request, id):
    reco = Recordatorio().buscar(id)
    variables = {
        'reco':reco
    }

    if request.method == 'POST':
        rec = Recordatorio()
        rec.id = request.POST.get('txtid')
        rec.diaPublicacion = timezone.now()
        rec.diaRecordatorio = request.POST.get('diaRecordatorio')
        rec.horaRecordatorio = request.POST.get('horaRecordatorio')
        rec.titulo = request.POST.get('titulo')
        rec.asunto = request.POST.get('asunto')
        rec.descripcion = request.POST.get('descripcion')
        user = request.user
        rec.autor = user
        try:
            rec.modificar()
            messages.success(request,'true')
        except:
            messages.error(request,'false')
        return redirect('/listar')

    return render(request, 'digital_calendar/CRUD/modificar.html', variables)
