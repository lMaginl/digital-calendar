from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('registro/', views.registro, name='registro'),
    path('iniciar_sesion/', views.iniciar_sesion, name='iniciar_sesion'),
    path('salir/', views.salir, name='salir'),
    path('listar/', views.listar, name='listar'),
    path('eliminar/<id>/', views.eliminar, name='eliminar'),
    path('modificar/<id>/', views.modificar, name='modificar'),
    path('home/', views.home, name='home'),
    path('perfil/', views.perfil, name='perfil'),
]