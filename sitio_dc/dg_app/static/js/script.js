$("#btnRegistrar").click(function() {
  if ($("#nombre").val() == "") {
    swal ( "Error" ,  "Ingrese Nombre" ,  "error" )
    return false;
  }
  if ($("#nombre").val().length < 3) {
    swal ( "Error" ,  "El nombre debe tener como mínimo 3 caracteres" ,  "warning" )
    return false;
  }
  if ($("#apellido").val() == "") {
    swal ( "Error" ,  "Ingrese Apellido" ,  "error" )
    return false;
  }
  if ($("#apellido").val().length < 3) {
    swal ( "Error" ,  "El apellido debe tener como mínimo 3 caracteres" ,  "warning" )
    return false;
  }
  if ($("#usuario").val() == "") { 
    swal ( "Error" ,  "Ingrese Usuario" ,  "error" )
    return false;
  }
  if ($("#usuario").val().length < 5) {
    swal ( "Error" ,  "El usuario debe tener como mínimo 5 caracteres" ,  "warning" )
    return false;
  }
  if ($("#pass1").val() == "") {
    swal ( "Error" ,  "Ingrese Contraseña" ,  "error" )
    return false;
  }
  if ($("#pass1").val().length < 6) {
    swal ( "Error" ,  "La contraseña debe tener como mínimo 6 caracteres" ,  "warning" )
    return false;
  }
  if ($("#pass2").val() == "") {
    swal ( "Error" ,  "Ingrese Repetir Contraseña" ,  "error" )
    return false;
  }
  if ($("#pass2").val().length < 6) {
    swal ( "Error" ,  "La confirmación de la contraseña debe tener como mínimo 6 caracteres" ,  "warning" )
    return false;
  }
  if ($("#pass1").val() != $("#pass2").val()) {
    swal ( "Error" ,  "Las Contraseñas no Coinciden" ,  "warning" )
    return false;
  }
  if ($("#mail").val() == "") {
    swal ( "Error" ,  "Ingrese Correo" ,  "error" )
    return false;
  }
  return true;
});

$("#btnLogin").click(function() {
  if ($("#usuarioLogin").val() == "") {
    swal ( "Error" ,  "Ingrese Usuario" ,  "error" )
    return false;
  }
  if ($("#usuarioLogin").val().length < 5) {
    swal ( "Error" ,  "El usuario debe tener como mínimo 5 caracteres" ,  "warning" )
    return false;
  }
  if ($("#passLogin").val() == "") {
    swal ( "Error" ,  "Ingrese Contraseña" ,  "error" )
    return false;
  }
  if ($("#passLogin").val().length < 6) {
    swal ( "Error" ,  "La contraseña debe tener como mínimo 6 caracteres" ,  "warning" )
    return false;
  }
  return true;
});

jQuery(document).ready(function() {
  "use strict";
  $("#slider-carousel").carouFredSel({
    responsive: true,
    width: "100%",
    circular: true,
    scroll: {
      items: 1,
      duration: 500,
      pauseOnHover: true
    },
    auto: true,
    items: {
      visible: {
        min: 1,
        max: 1
      },
      height: "variable"
    },
    pagination: {
      container: ".sliderpager",
      pageAnchorBuilder: false
    }
  });
  $(window).scroll(function() {
    var top = $(window).scrollTop();
    if (top >= 60) {
      $("header").addClass("header-second");
    } else if ($("header").hasClass("header-second")) {
      $("header").removeClass("header-second");
    }
  });
});
