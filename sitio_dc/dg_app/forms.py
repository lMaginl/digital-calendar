from django import forms
from django.forms import ModelForm
from .models import Recordatorio

class RecordatorioForm(ModelForm):
    class Meta:
        model = Recordatorio
        fields = ['diaRecordatorio','horaRecordatorio','titulo','asunto','descripcion']