import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestRecorddatorio:

    def test_modelo(self):
        reco = mixer.blend('dg_app.Recordatorio')
        assert reco.pk == 1, 'Error en el test'

    def test_str(self):
        tittle = "Titulo del Recordatorio"
        reco = mixer.blend('dg_app.Recordatorio', titulo=tittle)
        assert str(reco) == tittle

    def test_crear(self):
        reco = mixer.blend('dg_app.Recordatorio')
        assert reco.crear() == True

    def test_buscar(self):
        reco = mixer.blend('dg_app.Recordatorio')
        reco.crear()
        rec = mixer.blend('dg_app.Recordatorio')
        rec.buscar(reco.id)
        assert rec == reco

    def test_modificar(self):
        update = 'Tittle Update'
        reco = mixer.blend('dg_app.Recordatorio')
        reco.titulo = update
        assert reco.modificar() == True

    def test_eliminar(self):
        reco = mixer.blend('dg_app.Recordatorio')
        assert reco.eliminar(reco.id)
